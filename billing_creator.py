import psycopg2
import psycopg2.extras
from config import *
from requests import get
USERS_COUNT = 5

def pg_connect():
    conn = psycopg2.connect(dbname=PG_DB_NAME,
                            user=PG_USER,
                            host=PG_IP,
                            port=PG_PORT,
                            password=PG_PASSWORD)
    conn.autocommit = True
    return conn

if __name__ == "__main__":
    conn = pg_connect()
    cursor = conn.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)

    cursor.execute("SELECT * FROM \"user\" WHERE is_active = true AND is_deleted = false LIMIT %d" % USERS_COUNT)
    users = cursor.fetchall()
    for u in users:
        print(u.login)

        url = "https://{ip}:{port}/billmgr?authinfo={root}:{root_pwd}&func=account.edit&sok=ok&" \
              "email={email}&country={country}&realname={email}&passwd={pwd}&confirm={pwd}" \
              "&out=xjson".format(ip=BILLING_IP,
                                  port=BILLING_PORT,
                                  root=BILLING_USER,
                                  root_pwd=BILLING_PASSWORD,
                                  email=u.login,
                                  country=182,
                                  pwd='HufbjwkgyufRdtrdfeuwbftRFdftgvy'
                                  )
        r = get(url=url,verify=False)
        print(r.text)