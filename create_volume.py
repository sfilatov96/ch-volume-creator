from config import *
import psycopg2
import psycopg2.extras
import datetime as DT
from hashlib import sha256
from requests import post


def pg_connect():
    conn = psycopg2.connect(dbname=PG_DB_NAME,
                            user=PG_USER,
                            host=PG_IP,
                            port=PG_PORT,
                            password=PG_PASSWORD)
    conn.autocommit = True
    return conn


def send_to_gluster_api(user_id):
    data = {
        "event": "VOLUME_CREATE",
        "message": {
            "label": "default-volume",
            "user_id": user_id,
            "name": sha256(("default-volume"+user_id).encode("utf8")).hexdigest(),
            "size": 5,
            "replicas": 2
        }
    }
    url = ("http://%s/volumes" % GLUSTER_API)
    r = post(url=url, json=data)
    if r.status_code != 200:
        print(r.status_code, r.content.decode("utf8"))


if __name__ == "__main__":
    conn = pg_connect()
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

    cursor.execute("SELECT * FROM namespace_tariff  LIMIT %d" % USERS_COUNT)
    users = cursor.fetchall()
    for u in users:
        print("user_id=%s" % u)
        #send_to_gluster_api(u.user_id)